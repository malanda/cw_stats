var config = require("config");
var createError = require('http-errors');
var express = require('express');
var dba = require('./db');
const logger = require('./helpers/logger.js');
var morgan = require('morgan')
var indexRouter = require('./routes/index');
var playersRouter = require('./routes/players');
var clanRequest = require('./api/clan_request.js');
var warLogRequest = require('./api/war_log_request.js');
var compression = require('compression');
const assert = require('assert');

dba.db.on('connected', function() {
  logger.info(`BD conectada a ${config.mongodb.uri}`)
  clanRequest.get();
  warLogRequest.get();
  var app = express();
  app.use(morgan('tiny'))
  app.use(compression())
  app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
  app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
  app.use('/js', express.static(__dirname + '/node_modules/popper.js/dist'));
  app.use('/js', express.static(__dirname + '/node_modules/moment/min'));
  app.use('/js', express.static(__dirname + '/node_modules/chart.js/dist'));
  app.use('/js', express.static(__dirname + '/node_modules/tether/dist/js'));
  app.use('/js', express.static(__dirname + '/node_modules/progressbar.js/dist'));
  app.use('/js', express.static(__dirname + '/public/js'));
  app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
  app.use('/css', express.static(__dirname + '/public/css'));
  app.use('/images', express.static(__dirname + '/public/images'));
  app.set('views', __dirname +  '/views');
  app.set('view engine', 'pug');

  app.use('/', indexRouter);
  app.use('/players', playersRouter);

  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    next(createError(404));
  });

  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });

  app.listen(config.express.port, function() {
    logger.info(`Server started on port: ${config.express.port}.`);
  });
});

dba.db.on("error", function(err) {
  logger.info(`Error al conectarse: ${err}`);
});

dba.db.on('disconnected', function () {
  logger.info('BD se ha desconectado');
});

dba.connect(config.mongodb.uri);
