  var db = require('../db.js');
var Member = require('../models/member.js');
var playerRequest = require('../api/player_request.js');
var chestsRequest = require('../api/player_chests_request.js');
var ClanHistory = require('../models/clan_history.js');
const reducer = require('../helpers/reducer.js');
const beautify = require('js-beautify').html
const pug = require('pug');

function getMemberTimeSeries(tag, results) {
  ClanHistory.find().elemMatch('members', {'tag': tag}).select('id members.$').then((history) => {
    var memberTimeSeries = history.map((member) => {
      return{
        id: member.id,
        trophies: member.members[0].trophies
      }
    })
    results(reducer(memberTimeSeries));
  })
}

function getMember(name) {
  return Member.findOne({
    name: name
  });
}

exports.getPlayerInfo = (req, res, next) => {
  getMember(req.params.name).then((memberRaw) => {
    getMemberTimeSeries(memberRaw.tag, function(memberTimeSeries){
      playerRequest.get(memberRaw.tag).then((player) => {
        chestsRequest.get(memberRaw.tag).then((chests) => {
          delete player.achievements;
          const template = pug.compileFile(__dirname + '/../views/member.pug');
          var html = template({
            player: player,
            history: memberTimeSeries,
            chests: chests
          })
          res.send(beautify(html));
        });
      });
    });
  })
}
