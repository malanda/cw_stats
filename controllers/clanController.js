var date_formatter = require('../helpers/date_formatter.js');
const reducer = require('../helpers/reducer.js');
const assert = require('assert');
var db = require('../db.js');
var Clan = require('../models/clan.js');
var ClanHistory = require('../models/clan_history.js');
const beautify = require('js-beautify').html
const pug = require('pug');

function getClanTimeSeries(results) {
  ClanHistory.find({}).sort({
    id: 1
  }).then((histories) => {
    var x =histories.map((history) => {
      return {
        dates: history.id,
        donations: history.donations,
        scores: history.score
      }
    });
    results(reducer(x));
  })
}

function getClan() {
  return Clan.findOne({}).populate('members').exec();
}

exports.getClanInfo = (req, res, next) => {
  getClan().then((clan) => {
    getClanTimeSeries((histories) => {
      const template = pug.compileFile(__dirname + '/../views/clan.pug');
      var html = template({
        title: 'Clan',
        message: 'Clan wars',
        clan: clan,
        clanHist: histories
      })
      res.send(beautify(html));
    })
  })
}
