module.exports = function reducer(data) {
  const result = data.reduce(function(r, e) {
    return Object.keys(e).forEach(function(k) {
      if (!r[k]) r[k] = [].concat(e[k])
      else r[k] = r[k].concat(e[k])
    }), r
  }, {})
  return result;
}
