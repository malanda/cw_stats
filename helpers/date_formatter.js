module.exports = function formattedDate(epoch) {
  var options = {month: '2-digit', day: '2-digit' };
  return new Date(epoch).toLocaleString("es-MX", options);
}
