var mongoose = require('mongoose');
const assert = require('assert');
mongoose.Promise = global.Promise;
const db = mongoose.connection;
const logger = require('./helpers/logger.js');

exports.connect = function(url) {
  mongoose.connect(url, { useNewUrlParser: true }, function(error){
    assert.equal(null, error);
  });
}

exports.close = function(done) {
  mongoose.connection.close();
}

exports.db = db;
