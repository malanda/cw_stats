var config = require("config");
var client = require('./client.js');

exports.get = function(tag) {
  return client.get(config.api.player_url + tag);
}
