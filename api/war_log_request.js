var config = require("config");
const logger = require('../helpers/logger.js');
var client = require('./client.js');
var Statistic = require('../models/statistic.js');
var WarLog = require('../models/war_log.js');
const assert = require('assert');

exports.get = function() {
  client.get(config.api.clan_warlog_url)
    .then((clanWarLog) => {
      insertStatistic(clanWarLog);
    })
}

function insertWarLog(war) {
  WarLog.findOneAndUpdate({
    createdDate: war.createdDate
  }, war, {
    new: true,
    upsert: true
  }, function(error, doc) {
    assert.equal(null, error);
    logger.info(`Agregada guerra ${doc.createdDate}`);
  })
}

function insertStatistic(clanWarLog) {
  clanWarLog.forEach((war) => {
    insertWarLog(war);
    war.participants.forEach((participant) => {
      var stats = new stat(war.createdDate, participant)
      Statistic.findOneAndUpdate({
        _id: stats._id
      }, stats, {
        new: true,
        upsert: true
      }, function(error, doc) {
        assert.equal(null, error);
      });
    });
    logger.info(`Agregada estadistica de guerra ${war.createdDate}`);
  });
}

const stat = function(war_id, participant) {
  this._id = `${war_id}-${participant.tag}`
  this.war = war_id
  this.tag = participant.tag;
  this.name = participant.name;
  this.battlesPlayed = participant.battlesPlayed;
  this.wins = participant.wins;
};


setInterval(this.get, 600000);
