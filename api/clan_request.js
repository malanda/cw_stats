var config = require("config");
const logger = require('../helpers/logger.js');
var client = require('./client.js');
var Member = require('../models/member.js');
var Clan = require('../models/clan.js');
var Battle = require('../models/battle.js');
var historyInstance = require('../models/history_instance.js');
var ClanHistory = require('../models/clan_history');
const assert = require('assert');

exports.get = function() {
  client.get(config.api.clan_url)
    .then((clan) => {
      insertClan(clan);
    })
}

function insertClan(clan) {
  var members = clan.members;
  clan.members = [];
  clan._id = clan.tag;
  insertHistory(clan, members)
  Promise.all(insertMembers(members)).then(tags => {
    clan.members = tags;
    Clan.findOneAndUpdate({
      _id: clan._id
    }, clan, {
      new: true,
      upsert: true
    }, async function(error, doc) {
      assert.equal(null, error);
      logger.info(`Clan ${clan.name} actualizado`);
      // una por una para evitar el rate limit
      // TODO: ¿Se podra hacer de otra forma?
      for (var i = 0; i < tags.length; i++) {
        const res = await client.get(`${config.api.player_url}${tags[i]}/battles`)
        battlesDataRequestSuccess(res, tags[i]);
      }
    });
  });
};

// Serie de tiempo de los valores del clan
function insertHistory(clan, members) {
  var history = new ClanHistory(new historyInstance(clan, members));
  history.save(function(error) {
    assert.equal(null, error);
    logger.info(`Agregaga historia: ${history.id}`);
  });
}

// Insertar/actualizar miembros
function insertMembers(clanMembers) {
  return clanMembers.map(member => {
    return new Promise(function(resolve, reject) {
      member._id = member.tag;
      Member.findOneAndUpdate({
        _id: member._id
      }, member, {
        new: true,
        upsert: true
      }, function(error, doc) {
        assert.equal(null, error);
        logger.info(`${doc.name} agregado`)
        resolve(doc._id);
      })
    });
  });
};

// Agregar batallas de los miembros,
function battlesDataRequestSuccess(battles, tag) {
  battles.forEach((battleRaw) => {
    battleRaw.id = `${battleRaw.utcTime}-${tag}`;
    battleRaw.member = tag;
    Battle.findOneAndUpdate({
      id: battleRaw.id
    }, battleRaw, {
      new: true,
      upsert: true
    }, function(error, doc) {
      assert.equal(null, error);
    })
  })
  logger.info(`Actualizadas ${battles.length} batallas de ${tag}`);
}

setInterval(this.get, 3600000);
