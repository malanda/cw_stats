var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var db = require('../db');

var clanSchema = new Schema({
  tag: String,
  name: String,
  description: String,
  type: String,
  score: Number,
  memberCount: Number,
  requiredScore: Number,
  donations: Number,
  badge: {
    name: String,
    category: String,
    id: Number,
    image: String
  },
  location: {
    name: String,
    isCountry: Boolean,
    code: String
  },
  members: [{
    type: String,
    ref: 'Member'
  }],
  _id: String
});

var Clan = db.db.model('Clan', clanSchema);
module.exports = Clan;
