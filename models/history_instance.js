const History = function(clanData, membersData) {
  this.id = Date.now();
  this.donations = clanData.donations,
  this.memberCount = clanData.memberCount,
  this.score = clanData.score,
  this.members = membersData
};

module.exports = History;
