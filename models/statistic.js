var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var db = require('../db');

var statisticSchema = new Schema({
  _id: String,
  war: Number,
  tag: String,
  name: String,
  battlesPlayed: Number,
  wins: Number
}, {
  versionKey: false,
  capped: {
    size: 210000000
  }
});

// Se puede convertir la tabla anterior a capped para poder usar los datos guardados anteriormente.
// db.runCommand( { convertToCapped: 'statistics', size: 210000000 } )

var Statistic = db.db.model('Statistic', statisticSchema);
module.exports = Statistic;
