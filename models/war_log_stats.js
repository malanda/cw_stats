const WarLogStats = function(name, warData) {
  this.name = name;
  var recordStatistics = computeRecordStatistic(warData);
  this.wins = recordStatistics.wins;
  this.losses = recordStatistics.losses;
  this.missing = recordStatistics.missing;
  this.winRatio = Math.round(computeWinRatio(recordStatistics));
  this.active = !(this.wins === 0 && this.losses === 0 && this.missing === 0);
}

function computeRecordStatistic(warData) {
  return warData.reduce(function(previousValue, currentValue) {
    var losses = computeLosses(currentValue.battlesPlayed, currentValue.wins);
    var missing = computeMissing(currentValue.battlesPlayed);

    return {
      wins: previousValue.wins + currentValue.wins,
      losses: previousValue.losses + losses,
      missing: previousValue.missing + missing
    }
  }, {wins: 0, battlesPlayed: 0, losses: 0, missing: 0});
}

function computeLosses(battlesPlayed, wins) {
  return (battlesPlayed != wins) ? battlesPlayed - wins : 0
}

function computeMissing(battlesPlayed) {
  return (battlesPlayed === 0) ? 1 : 0;
}

function computeWinRatio(recordStatistics) {
  var totalBattles = recordStatistics.wins + recordStatistics.losses + recordStatistics.missing;
  return (totalBattles !== 0) ? recordStatistics.wins / totalBattles * 100 : 0;
}

module.exports = WarLogStats;
